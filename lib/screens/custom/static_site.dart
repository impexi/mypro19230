import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:webview_flutter/webview_flutter.dart';
import 'package:provider/provider.dart';
import '../custom/smartchat.dart';
import '../../models/app.dart';
import '../../generated/l10n.dart';

class StaticSite extends StatelessWidget {
  final String data;
  final bool showChat;

  StaticSite({this.data, this.showChat});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: showChat
          ? SmartChat(
              margin: EdgeInsets.only(
                right:
                    Provider.of<AppModel>(context, listen: false).locale == 'ar'
                        ? 30.0
                        : 0.0,
              ),
            )
          : Container(),
      body: Platform.isMacOS || Platform.isWindows || Platform.isFuchsia
          ? Center(
              child: Text(S.of(context).thisPlatformNotSupportWebview),
            )
          : WebView(
              onWebViewCreated: (controller) async {
                final String contentBase64 =
                    base64Encode(const Utf8Encoder().convert(data));
                await controller
                    .loadUrl('data:text/html;base64,$contentBase64');
              },
            ),
    );
  }
}

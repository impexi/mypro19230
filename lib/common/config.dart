export 'config/advertise.dart';
export 'config/general.dart';
export 'config/onboarding.dart';
export 'config/payments.dart';
export 'config/products.dart';
export 'config/smartchat.dart';

/// Server config demo for WooCommerce
/// Get more example for Opencart / Magento / Shopify from the example folder
const serverConfig = {
  "type": "woo",
  "url": "https://www.sneakers-point.com",

  /// document..
  "consumerKey": "ck_ba1ce7028364fc4d28508035879727aca9f0ffe8",
  "consumerSecret": "cs_81d5cd6b466475b5eba95651f509db5cddf255ce",

  /// Your website woocommerce. You can remove this line if it same url
  "blog": "https://www.sneakers-point.com",

  /// set blank to use as native screen
  "forgetPassword": "https://www.sneakers-point.com/my-account-2"
};

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

import '../../common/constants.dart';
import '../../generated/l10n.dart';
import '../../models/product/product.dart';
import '../../widgets/product/product_card_view.dart';
import "../home/vertical/pinterest_card.dart";
import 'product_list_tile.dart';

class ProductList extends StatefulWidget {
  final List<Product> products;
  final bool isFetching;
  final bool isEnd;
  final String errMsg;
  final width;
  final padding;
  final String layout;
  final Function onRefresh;
  final Function onLoadMore;

  ProductList({
    this.isFetching = false,
    this.isEnd = true,
    this.errMsg,
    this.products,
    this.width,
    this.padding = 8.0,
    this.onRefresh,
    this.onLoadMore,
    this.layout = "list",
  });

  @override
  _ProductListState createState() => _ProductListState();
}

class _ProductListState extends State<ProductList> {
  RefreshController _refreshController;

  List<Product> emptyList = [
    Product.empty('1'),
    Product.empty('2'),
    Product.empty('3'),
    Product.empty('4'),
    Product.empty('5'),
    Product.empty('6')
  ];

  @override
  initState() {
    super.initState();

    /// if there are existing product from previous navigate we don't need to enable the refresh
    _refreshController = RefreshController(initialRefresh: false);
  }

  _onRefresh() async {
    if (!widget.isFetching) {
      widget.onRefresh();
    }
  }

  _onLoading() async {
    print('onLoading');
    if (!widget.isFetching) {
      widget.onLoadMore();
    }
  }

  @override
  void didUpdateWidget(ProductList oldWidget) {
    super.didUpdateWidget(oldWidget);

    if (widget.isFetching == false && oldWidget.isFetching == true) {
      _refreshController.refreshCompleted();
      _refreshController.loadComplete();
    }
  }

  @override
  void dispose() {
    _refreshController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final screenSize = MediaQuery.of(context).size;
    final isTablet = widget.width / screenSize.height > 1.2;
    final widthScreen = widget.width != null ? widget.width : screenSize.width;
    var widthContent = 0.0;
    var crossAxisCount = 1;
    var childAspectRatio = 0.5;

    if (widget.layout == "card") {
      crossAxisCount = isTablet ? 2 : 1;
      childAspectRatio = 0.65;
      widthContent =
          isTablet ? 0.75 * widthScreen / 2 : widthScreen; //one column
    } else if (widget.layout == "columns") {
      crossAxisCount = isTablet ? 4 : 3;
      childAspectRatio = isTablet ? 0.6 : 0.5;
      widthContent =
          isTablet ? 0.78 * widthScreen / 5 : (widthScreen / 3); //three columns
    } else if (widget.layout == "listTile") {
      crossAxisCount = isTablet ? 2 : 1;
      childAspectRatio = 1.7;
    } else {
      /// 2 columns on mobile, 3 columsn on ipad
      crossAxisCount = isTablet ? 3 : 2;
      childAspectRatio = isTablet ? 0.75 : 0.5;
      //layout is list
      widthContent =
          isTablet ? 0.78 * widthScreen / 4 : (widthScreen / 2); //two columns
    }

    final productsList = (widget.products == null || widget.products.isEmpty) &&
            widget.isFetching
        ? emptyList
        : widget.products;

    if (productsList == null || productsList.isEmpty) {
      return Center(
          child: Text(S.of(context).noProduct,
              style: TextStyle(color: Colors.black)));
    }

    return SmartRefresher(
      header: MaterialClassicHeader(
        backgroundColor: Theme.of(context).backgroundColor,
      ),
      enablePullDown: true,
      enablePullUp: !widget.isEnd,
      controller: _refreshController,
      onRefresh: _onRefresh,
      onLoading: _onLoading,
      footer: kCustomFooter(context),
      child: widget.layout != "pinterest"
          ? GridView.builder(
              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: crossAxisCount,
                childAspectRatio: childAspectRatio,
              ),
              cacheExtent: 500.0,
              itemCount: productsList.length,
              itemBuilder: (context, i) {
                if (widget.layout == "listTile") {
                  return ProductItemTileView(item: productsList[i]);
                } else {
                  return Container(
                    width: 100,
                    child: ProductCard(
                      item: productsList[i],
                      showCart: widget.layout != "columns",
                      showHeart: true,
                      width: widthContent,
                      marginRight: widget.layout == "card" ? 0.0 : 10.0,
                      // tablet: widget.width / screenSize.height > 1.2,
                    ),
                  );
                }
              },
            )
          : StaggeredGridView.countBuilder(
              crossAxisCount: 4,
              mainAxisSpacing: 4.0,
              shrinkWrap: true,
              primary: false,
              crossAxisSpacing: 4.0,
              itemCount: productsList.length,
              itemBuilder: (context, index) => PinterestCard(
                item: productsList[index],
                showOnlyImage: false,
                width: MediaQuery.of(context).size.width / 2,
                showCart: false,
              ),
              staggeredTileBuilder: (index) => StaggeredTile.fit(2),
            ),
    );
  }
}

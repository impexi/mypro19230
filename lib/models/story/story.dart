import 'package:flutter/material.dart';

class Story {
  int layout;
  String urlImage;
  List<StoryContent> contents;

  Story({
    this.layout,
    this.urlImage,
    this.contents,
  });

  @override
  String toString() {
    return '''\nStory{
      \tlayout: $layout
      \turlImage: $urlImage
      \contents: ${contents.toString()}
    }
    ''';
  }

  Story.fromJson(Map<dynamic, dynamic> json) {
    layout = json['layout'] ?? '';
    urlImage = json['urlImage'] ?? '';
    contents = [];
    if (json['contents'] != null && json['contents'].isNotEmpty) {
      for (var item in json['contents']) {
        contents.add(StoryContent.fromJson(item));
      }
    }
  }

  Map<String, dynamic> toJson() {
    return {
      'layout': layout ?? '',
      'urlImage': urlImage ?? '',
      'contents': contents != null && contents.isNotEmpty
          ? contents.map((content) => content.toJson()).toList()
          : []
    };
  }
}

class StoryContent {
  String title;
  Offset positionTitle;
  StoryLink link;
  StoryTypography typography;
  StoryAnimation animation;
  StorySpacing spacing;

  StoryContent({
    this.title,
    this.positionTitle,
    this.link,
    this.typography,
    this.animation,
    this.spacing,
  });

  @override
  String toString() {
    return '''StoryConfig{
      title: $title
      positionTitle: ${positionTitle.toString()}
      link: ${link.toString()}
      typography: ${typography.toString()}
      animation: ${animation.toString()}
      spacing: ${spacing.toString()}
    }
    ''';
  }

  StoryContent.fromJson(Map<dynamic, dynamic> json) {
    title = json['title'] ?? '';

    if (json['link'] != null) {
      link = StoryLink.fromJson(json['link']);
    }

    if (json['typography'] != null) {
      typography = StoryTypography.fromJson(json['typography']);
    }

    if (json['animation'] != null) {
      animation = StoryAnimation.fromJson(json['animation']);
    }

    if (json['spacing'] != null) {
      spacing = StorySpacing.fromJson(json['spacing']);
    }

    if (json['positionTitle'] != null) {
      positionTitle =
          Offset(json['positionTitle']['dx'], json['positionTitle']['dy']);
    } else {
      positionTitle = Offset(100, 100);
    }
  }

  Map<String, dynamic> toJson() {
    return {
      'title': title ?? '',
      'link': link ?? '',
      'typography': typography ?? '',
      'animation': animation ?? '',
      'spacing': spacing ?? '',
      'positionTitle': positionTitle ?? '',
    };
  }
}

class StoryLink {
  String url;
  String type;

  StoryLink({
    this.url,
    this.type,
  });

  @override
  String toString() {
    return '''StoryLink{
      url: $url
      type: $type
    }
    ''';
  }

  StoryLink.fromJson(Map<dynamic, dynamic> json) {
    url = json['url'] ?? '';
    type = json['type'] ?? '';
  }

  Map<String, dynamic> toJson() {
    return {
      'url': url ?? '',
      'type': type ?? '15',
    };
  }
}

class StoryTypography {
  String font;
  double fontSize;
  String fontStyle;
  String align;
  String transform;

  StoryTypography({
    this.font,
    this.fontSize,
    this.fontStyle,
    this.align,
    this.transform,
  });

  @override
  String toString() {
    return '''StoryTypography{
      type: $font
      fontSize: $fontSize
      fontStyle: $fontStyle
      align: $align
      transform: $transform
    }
    ''';
  }

  StoryTypography.fromJson(Map<dynamic, dynamic> json) {
    font = json['font'] ?? '';
    fontSize = json['fontSize'] ?? '';
    fontStyle = json['fontStyle'] ?? '';
    align = json['align'] ?? '';
    transform = json['transform'] ?? '';
  }

  Map<String, dynamic> toJson() {
    return {
      'type': font ?? '',
      'fontSize': fontSize ?? 15,
      'fontStyle': fontStyle ?? '',
      'align': align ?? '',
      'transform': transform ?? ''
    };
  }
}

class StoryAnimation {
  String type;
  int milliseconds;
  int delaySecond;

  StoryAnimation({
    this.type,
    this.milliseconds,
    this.delaySecond,
  });

  @override
  String toString() {
    return '''StoryAnimation{
      type: $type
      milliseconds: $milliseconds
      delaySecond: $delaySecond
    }
    ''';
  }

  StoryAnimation.fromJson(Map<dynamic, dynamic> json) {
    type = json['type'] ?? '';
    milliseconds = json['milliseconds'] ?? 300;
    delaySecond = json['delaySecond'] ?? 0;
  }

  Map<String, dynamic> toJson() {
    return {
      'type': type ?? '',
      'milliseconds': milliseconds ?? 300,
      'delaySecond': delaySecond ?? 0,
    };
  }
}

class StorySpacing {
  double left;
  double right;
  double top;
  double bottom;
  StorySpacing({
    this.left,
    this.right,
    this.top,
    this.bottom,
  });

  @override
  String toString() {
    return '''StorySpacing{
      left: $left
      right: $right
      top: $top
      bottom: $bottom
    }
    ''';
  }

  StorySpacing.fromJson(Map<dynamic, dynamic> json) {
    left = json['left'] ?? '';
    right = json['right'] ?? '';
    top = json['top'] ?? '';
    bottom = json['bottom'] ?? '';
  }

  Map<String, dynamic> toJson() {
    return {
      'left': left ?? '',
      'right': right ?? '',
      'top': top ?? '',
      'bottom': bottom ?? '',
    };
  }
}

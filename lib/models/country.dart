import 'package:localstorage/localstorage.dart';
import 'package:country_pickers/country_pickers.dart';

class State {
  String id;
  String code;
  String name;
  State({this.id, this.code, this.name});

  State.fromConfig(dynamic parsedJson) {
    if (parsedJson is Map) {
      id = parsedJson["code"];
      code = parsedJson["code"];
      name = parsedJson["name"];
    }
    if (parsedJson is String) {
      id = parsedJson;
      code = parsedJson;
      name = parsedJson;
    }
  }

  State.fromMagentoJson(Map<String, dynamic> parsedJson) {
    id = parsedJson["id"];
    code = parsedJson["code"];
    name = parsedJson["name"];
  }
}

class Country {
  String id;
  String name;
  String icon;
  List<State> states = [];

  Country({this.id, this.name, this.states});

  Country.fromConfig(this.id, this.name, this.icon, List states) {
    name =
        name != null ? name : CountryPickerUtils.getCountryByIsoCode(id).name;
    if (states != null) {
      for (var item in states) {
        states.add(State.fromConfig(item));
      }
    }
  }

  Country.fromMagentoJson(Map<String, dynamic> parsedJson) {
    id = parsedJson["id"];
    name = parsedJson["full_name_english"] ?? parsedJson["full_name_locale"];
    final regions = parsedJson["available_regions"];
    if (regions != null) {
      for (var item in regions) {
        states.add(State.fromMagentoJson(item));
      }
    }
  }

  Map<String, dynamic> toJson() {
    return {"id": id, "name": name, "states": states};
  }

  Country.fromLocalJson(Map<String, dynamic> json) {
    try {
      id = json['id'];
      name = json['name'];
      states = json['states'];
    } catch (e) {
      print(e.toString());
    }
  }

  Future<void> saveToLocal() async {
    final LocalStorage storage = LocalStorage("address");
    try {
      final ready = await storage.ready;
      if (ready) {
        await storage.setItem('', toJson());
      }
    } catch (err) {
      print(err);
    }
  }
}

class ListCountry {
  List<Country> list = [];

  ListCountry({this.list});

  ListCountry.fromMagentoJson(List json) {
    if (json != null && json is List) {
      for (var item in json) {
        if (item["full_name_locale"] != null &&
            item["full_name_english"] != null &&
            item["id"] != null) {
          list.add(Country.fromMagentoJson(item));
        }
      }
    }
  }
}
